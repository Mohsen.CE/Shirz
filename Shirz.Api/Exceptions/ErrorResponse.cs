using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Shirz.Api.Exceptions
{
    public class ErrorResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public ErrorResponse(Exception ex, IWebHostEnvironment env)
        {
            Type = ex.GetType().Name;
            Message = ex.Message;
            if (env.IsDevelopment())
                StackTrace = ex.ToString();
        }
    }
}