using System;
using System.Threading.Tasks;
using AutoMapper;
using Shirz.Api.DTO;
using Shirz.Interfaces;
using Shirz.Models;
using Microsoft.AspNetCore.Mvc;


namespace Shirz.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService categoryService;
        private readonly IMapper mapper;

        public CategoryController(ICategoryService categoryService, IMapper mapper)
        {
            this.categoryService = categoryService;
            this.mapper = mapper;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            var categories = await categoryService.ListAsync();
            return Ok(categories);
        }

        //if SuperUser Authenticated:
        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody] AddCategoryRequest request)
        {
            var category = mapper.Map<AddCategoryRequest, Category>(request);
            await categoryService.Add(category);

            return StatusCode(200);
        }

        //if SuperUser Authenticated:
        [HttpDelete("{id}/delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await categoryService.Delete(id);
            return StatusCode(200);
        }

        //if SuperUser Authenticated:
        [HttpPut("{id}/update")]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateCategoryRequest request)
        {
            await categoryService.Update(id, request.Name, request.Description);
            return StatusCode(200);
        }
    }
}