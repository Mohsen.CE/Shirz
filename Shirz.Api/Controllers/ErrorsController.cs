using Shirz.Api.Exceptions;
using Shirz.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Shirz.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        private readonly IWebHostEnvironment env;
        private readonly ILogger<ErrorsController> logger;

        public ErrorsController(IWebHostEnvironment env, ILogger<ErrorsController> logger)
        {
            this.env = env;
            this.logger = logger;
        }

        [Route("error")]
        public ErrorResponse Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;
            var code = 500;

            if (exception is ShirzException) code = ((ShirzException)exception).Code;

            Response.StatusCode = code;
            logger.LogError(exception, $"Error in {env.EnvironmentName}");

            return new ErrorResponse(exception, env);
        }
    }
}