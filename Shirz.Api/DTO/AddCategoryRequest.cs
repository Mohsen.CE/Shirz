using System;

namespace Shirz.Api.DTO
{
    public class AddCategoryRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
    }
}