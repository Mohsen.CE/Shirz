using AutoMapper;
using Shirz.Api.DTO;
using Shirz.Models;

namespace Shirz.Api.Mapper
{
    public class MapProfiler : Profile
    {
        public MapProfiler()
        {
            CreateMap<AddCategoryRequest, Category>();
        }
    }
}