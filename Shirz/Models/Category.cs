using System;
using System.Collections.Generic;

namespace Shirz.Models
{
    public class Category : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public virtual Category Parent { get; set; }
        public virtual IList<Category> Children { get; set; }
    }
}