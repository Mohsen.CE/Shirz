using System;

namespace Shirz.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}