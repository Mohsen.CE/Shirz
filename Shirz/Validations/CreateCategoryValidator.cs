using System;
using System.Linq;
using FluentValidation;
using Shirz.DataAccess;
using Shirz.Models;

namespace Shirz.Validations
{
    public class CreateCategoryValidator : AbstractValidator<Category>
    {
        private readonly ShirzDbContext context;

        public CreateCategoryValidator(ShirzDbContext context)
        {
            this.context = context;

            RuleFor(cat => cat.Name)
                .NotEmpty()
                .WithMessage("Name can not be empty")
                .Must(notDuplicate)
                .WithMessage("Name is duplicate");

            RuleFor(cat => cat.Description)
                .MaximumLength(250)
                .WithMessage("Description must be less than 250 character");

        }

        private bool notDuplicate(string name)
        {
            return !context.Categories.Any(cat => cat.Name == name);
        }
    }
}