using Shirz.Models;
using Microsoft.EntityFrameworkCore;

namespace Shirz.DataAccess
{
    public class ShirzDbContext : DbContext
    {
        public ShirzDbContext(DbContextOptions<ShirzDbContext> options) :
            base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Category>()
                .HasOne(x => x.Parent)
                .WithMany(x => x.Children)
                .HasForeignKey(x => x.ParentId);
        }
    }
}