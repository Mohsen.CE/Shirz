using System;

namespace Shirz.Exceptions
{
    public class ShirzException : Exception
    {
        public int Code { get; set; }
        public ShirzException(int code, string message) :
            base(message)
        {
            this.Code = code;
        }
    }
}