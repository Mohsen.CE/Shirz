using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shirz.DataAccess;
using Shirz.Exceptions;
using Shirz.Interfaces;
using Shirz.Models;
using Shirz.Validations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Shirz.Services
{
    public class CategoryService : ICategoryService
    {
        private const string categoryCacheKey = "CategoryCacheKey";
        private readonly ShirzDbContext context;
        private readonly IMemoryCache cache;
        private readonly ILogger<CategoryService> logger;

        public CategoryService(ShirzDbContext context, IMemoryCache cache, ILogger<CategoryService> logger)
        {
            this.context = context;
            this.cache = cache;
            this.logger = logger;
        }

        public async Task Add(Category category)
        {
            CreateCategoryValidator validator = new CreateCategoryValidator(context);
            var valid = validator.Validate(category);
            if (!valid.IsValid)
                throw new ShirzException(400, valid.ToString("\n"));

            await context.Categories.AddAsync(category);
            await context.SaveChangesAsync();
            logger.LogInformation($"Category by Name: {category.Name} and Id: {category.Id} created");
            await UpdateCachedCategory();
        }

        public async Task Delete(Guid categoryId)
        {
            var category = await context.Categories
            .Include(root => root.Children)
            .FirstOrDefaultAsync(x => x.Id == categoryId);

            if (category == null)
                throw new ShirzException(404, $"Category {categoryId} is not exsist.");

            if (category.Children.Any())
                throw new ShirzException(403, $"Can not delete {categoryId}, you must first delete children.");

            context.Categories.Remove(category);
            await context.SaveChangesAsync();
            logger.LogInformation($"Category {categoryId} deleted.");
            await UpdateCachedCategory();
        }

        public async Task<List<Category>> ListAsync()
        {
            var categories = await this.cache.GetOrCreateAsync(categoryCacheKey, async factory =>
               {
                   var cats = await fetchCategories();
                   return cats;
               });

            return categories;
        }

        public async Task Update(Guid categoryId, string name, string description)
        {
            var category = await context.Categories.FindAsync(categoryId);
            if (category == null)
                throw new ShirzException(404, $"Category {categoryId} is not exsist.");

            UpdateCategoryValidator validator = new UpdateCategoryValidator(context);
            var valid = validator.Validate(category);
            if (!valid.IsValid)
                throw new ShirzException(400, valid.ToString("\n"));

            category.Name = name;
            category.Description = description;

            context.Categories.Update(category);
            await context.SaveChangesAsync();
            logger.LogInformation($"Category {categoryId} updated.");
            await UpdateCachedCategory();
        }

        private async Task UpdateCachedCategory()
        {
            var cats = await fetchCategories();
            this.cache.Set(categoryCacheKey, cats);
        }

        private Task<List<Category>> fetchCategories()
        {
            var categories = context.Categories
                            .Include(root => root.Children)
                            .Where(x => x.ParentId == null).ToListAsync();

            return categories;
        }
    }
}