using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shirz.Models;

namespace Shirz.Interfaces
{
    public interface ICategoryService
    {
        Task<List<Category>> ListAsync();
        Task Add(Category category);
        Task Delete(Guid categoryId);
        Task Update(Guid categoryId, string name, string description);
    }
}